# Supported file types

**bindet** currently supports the following file types:

- Zip
- Rar (4 and 5)
- Tar (uncompressed)
- LZMA
- 7zXZ
- Zst
- Png
- Jpg
- 7-zip
- Opus
- Vorbis
- Mp3
- Webp
- Flac
- Matroska (mkv, mka, mks, mk3d, webm)
- Wasm
- Java Class
- Scala Tasty
- Mach-O
- Elf (Executable and Linkable Format)
- Wav
- Avi
- Aiff
- Tiff
- Sqlite3 (.db)
- Ico
- Dalvik
- Pdf
- Exe/Dll
- Gif
- Xcf
- Scala Tasty
- Bmp
- Iso
- Gpg (Only Packet version 3) and Armored Gpg