# How the detection works (0.3.0)

**bindet** has 3 different detection methods:

- Basic
- StartScan
- FullScan

## Basic

Currently, not available, see [this issue](https://gitlab.com/Kores/bindet/-/issues/5).

But it is already implemented, it looks at the start of the data by reading the smallest possible number of bytes
given the file types you want to detect.

This is a very simple approach, but a bunch of file types has magic numbers that appear right at the start of the file,
so this is enough for most of the cases and is very good to detect a high amount of files *at once*.

### Example:

Magic number: `[0x4F, 0x40, 0x3F, 0x20]`
File bytes: `[0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ...]`

Since there is only a magic number to test against, it takes the first 4 bytes of the file and compares it to the magic number.

```
[0x4F, 0x40, 0x3F, 0x20]
 |  |  |  |  |  |  |  |  
[0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ...]
```

## StartScan

Reads the largest amount number of bytes at once, and then do a sliding window scan or a fixed window match, some file types
can only be detected by using this approach.

The start can also do the basic scan first because its costs are negligible, since the data that was read by the basic
detection are reused to do the start scan instead of reading them again.

### Example:

Magic number: `[0x4F, 0x40, 0x3F, 0x20]`
File bytes: `[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ...]`

In this case, the magic number is not at the start and the file type informs that we need to read at least 6 bytes to do
the detection, so it first tries to basic detection, but the file type detector does nothing, because it wants more bytes,
and then it takes more bytes and tries to match:

```
Basic:
[]

[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ...]

Nothing to do.

StartScan:
            [0x4F, 0x40, 0x3F, 0x20]
             |  |  |  |  |  |  |  |  
[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ...]
```

Note that StartScan is not necessarily a scan, depending on the type detector, 
it can do a simple match against a fixed range of bytes, like in the example above.

## FullScan

It does both the basic and start scan, and then a final backward sliding window detection against the bytes in the end of the file,
however, it only does the backward scan if the resulting detection of other steps is not conclusive about some file types,
so it calls those file types detector to conclude if the file is in fact a file of that type.

### Example:

Initial magic number: `[0x4F, 0x40, 0x3F, 0x20]`
End of file magic number: `[0x3D, 0x5B, 0x6A, 0x02]`
File bytes: `[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ..., 0x3D, 0x5B, 0x6A, 0x02, 0x1F]`

In this case, the magic number is not at the start of the file, like in the `StartScan` example, and the file type
informs that we need to read at least 6 bytes at the start, and if matching, do the backward sliding window at the end,
so the detection logic does this:

```
Basic:
[]

[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ..., 0x3D, 0x5B, 0x6A, 0x02, 0x1F]

Nothing to do.

StartScan:
            [0x4F, 0x40, 0x3F, 0x20]
             |  |  |  |  |  |  |  |  
[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ..., 0x3D, 0x5B, 0x6A, 0x02, 0x1F]

Matched as Maybe.

EndScan:

                                                           [0x3D, 0x5B, 0x6A, 0x02]
                                                            |  |  |  |  |  |  |  |  
[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ..., 0x3D, 0x5B, 0x6A, 0x02, 0x1F]

Not matched, slide to left.

                                                     [0x3D, 0x5B, 0x6A, 0x02]
                                                      |  |  |  |  |  |  |  |  
[0x78, 0x01, 0x4F, 0x40, 0x3F, 0x20, 0x50, 0x18, ..., 0x3D, 0x5B, 0x6A, 0x02, 0x1F]

Matched!
```
