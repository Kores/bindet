# Summary

- [What is bindet](./whatis.md)
- [Supported file types](./supported-file-types.md)
- [How the detection works](./how.md)