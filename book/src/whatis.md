# What is bindet

**bindet** is a binary detection library with focus on performance and reliability, it currently does not provide detection
for a broad range of file types, but it is able to detect some of the most common media types and some text files.

## How reliable is the detection

It depends on the selected detection mode, if you value more performance or reliability, and if you need to detect
a huge amount of files at once, you will be better with the most basic detection.

Also, bindet (currently) does not protect against any kind false positives, and some detections rely on Unicode valid bytes, which means
that files that contains Magic Number of file formats in the same position they appear on their specification will be detected as so[^1].

[^1]: In the future, bindet may provide a way to detect fully valid text files (Unicode only), this would be useful for disambiguating
      between text files and other file types when they coincidentally have a magic number in the same position.

It is important to note that **bindet** does not interpret the entire file type structure, like if the file entirely
conforms to the specification, so it may not be used to determine if the file is valid or not, but can be safely trusted
for categorization or other kinds of tasks that needs a basic detection.
